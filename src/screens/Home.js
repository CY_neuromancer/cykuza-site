import { Col, Container, Row } from "react-bootstrap"
import { Banner, Footer } from "../components"
import { arrow, c, cmobile } from "../components/Images"

function Home() {
    return <main>
	{/* banner section  */}
	<Banner />
	{/* end banner section  */}
  <Footer/>

  </main>
}

export default Home
