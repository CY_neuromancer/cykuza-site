import { Col, Container, Row } from "react-bootstrap"
import { AboutSection, Footer } from "../components"
import { arrow, c, cmobile } from "../components/Images"

function About() {
    return <main>
	{/* about section */}
	<AboutSection />
	{/* end about section  */}
  <Footer/>

  </main>
}

export default About
