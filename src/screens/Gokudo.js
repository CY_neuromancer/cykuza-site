import { Col, Container, Row } from "react-bootstrap"
import { Footer, GokudoSection } from "../components"
import { arrow, c, cmobile } from "../components/Images"

function Gokudo() {
    return <main>
	<GokudoSection />
	{/* end second section  */}
	<Footer/>

    </main>
}

export default Gokudo
