import { Col, Container, Row } from "react-bootstrap"
import '../assets/css/footer.css'
import { arrow } from "./Images"
function Footer() {
    return <>
	<footer>
	    <Container fluid>
		<div>

		    <div className="custom-row">
			<div className="custom-col">
			    <h3>CYKUZA</h3>
			    <ul className="ul">
				<li>
				    <a href="/about">ABOUT</a>
				</li>
				<li>
				    <a href="/gokudo">GOKUDO</a>

				</li>
			    </ul>
			</div>
			<div className="custom-col">
			    <h3>INFO</h3>
			    <ul className="ul">
				<li>
				    <a href="/todo" target="_blank">BLOG</a>
				</li>
				<li>
				    <a href="/news" target="_blank">NEWS</a>
				</li>
			    </ul>
			</div>
			<div className="custom-col">
			    <h3>DOCS</h3>
			    <ul className="ul">
				<li>
				    <a href="https://gitlab.com/cykuza/manifesto/" target="_blank" className="footer-active normal-btn-with-icon"><span>Manifesto</span> <img src={arrow} /></a>

				</li>
			    </ul>
			</div>
		    </div>
			<hr />
		</div>
		<div className="footer-bottom">
		    <div>
			<p className="text-white">&copy; {new Date().getFullYear()} CYkuza</p>
		    </div>
		    <ul className="social-links">
			<li>
			    <a href="https://t.me/cykuza" target="_blank"><i class="fa-brands fa-telegram"></i></a>
			</li>
			<li>
			    <a href="https://snort.social/p/npub1zxl8m0rwxeqtegv47eclvj3me7x2k00rg3gr26re002rmq49lr5slhll8t" target="_blank"><i class="fa-solid fa-hashtag"></i></a>
			</li>
			<li>
			    <a href="https://matrix.to/#/#cykuza:matrix.org" target="_blank"><i class="fa fa-matrix-org fa-lg"></i></a>
			</li>
			<li>
			    <a href="https://gitlab.com/cykuza/" target="_blank"><i class="fa-brands fa-gitlab"></i></a>
			</li>
		    </ul>
		</div>
	    </Container>
	</footer>
    </>
}

export default Footer
