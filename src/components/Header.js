import { Link, NavLink } from "react-router-dom"
import { closeicon, logo, showicon } from "./Images"
import '../assets/css/header.css'
import { memo, useContext, useState } from "react"
import GlobalState from "../Context"

function Header() {
    const [connect, setConnect] = useState(false)
    const { isShow, setIsShow } = useContext(GlobalState)
    return <>
	<header>

	    <div className="custom-header">
		<div className="head-flex">
		    <div className="logo-section">
			<Link to="/"><img src={logo} /></Link>
		    </div>
		    <img src={showicon} className="mobile-head-open" onClick={(e) => setIsShow(true)}/>
		</div>
		<div className="header-100-vh">
		<div className={isShow ? "header-body nav-open" : "header-body nav-close"}>
		    <div className="mobile-head">
			<img src={logo} className="sm-logo"/>
			<img src={closeicon} className="close-icon" onClick={(e) => setIsShow(false)}/>
		    </div>
		    <ul className="ul">
			<li className="custom-order-3">
			    <a href="https://gitlab.com/cykuza/manifesto/" target="_blank" className="active">Manifesto</a>
			</li>
			<li className="custom-order-1">
			    <div className="custom-dropdown">
				<a href="#" >CYkuza</a>
				<div className="dropdown-body">
				    <ul>
					<li>
					    <a href="/about">About</a>
					</li>
					<li>
					    <a href="/gokudo">Gokudo</a>
					</li>
				    </ul>
				</div>
			    </div>
			</li>
			<li className="custom-order-2">
			<div className="custom-dropdown">
			    <a href="#">Info</a>
			    <div className="dropdown-body">
				    <ul>
					<li>
					    <a href="/todo" target="_blank">Blog</a>
					</li>
					<li>
					    <a href="/news" target="_blank">News</a>
					</li>
				 </ul>
				</div>
			    </div>
			</li>
			<div className="custom-order-4 m-d-none">
			    <li >
				<a href="https://gitlab.com/cykuza/" target="_blank"><i class="fa-brands fa-gitlab"></i></a>
			    </li>
			    <li>
				<a href="https://matrix.to/#/#cykuza:matrix.org" target="_blank"><i class="fa fa-matrix-org fa-2x"></i></a>
			    </li>
			</div>
		    </ul>
		    <ul className="ul mobile-social">
			    <li >
				<a href="https://gitlab.com/cykuza/" target="_blank"><i class="fa-brands fa-gitlab"></i></a>
			    </li>
			    <li>
				<a href="https://matrix.to/#/#cykuza:matrix.org" target="_blank"><i class="fa fa-matrix-org fa-lg"></i></a>
			    </li>
		    </ul>
		</div>
		</div>
	    </div>
	</header>
    </>
}

export default memo(Header)
